package de.richtercloud.docker.java.file.readability;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

public class TheTest {

    @Test
    public void testSomeMethod() throws IOException {
        //test custom file
        String userHome = System.getProperty("user.home");
        System.out.println(String.format("system property user.home: %s",
                userHome));
        File file = new File(userHome, "file");
        if(!file.createNewFile()) {
            throw new IOException("test arrangement failed");
        }
        if(!file.setReadable(false)) {
            throw new IOException("test arrangement failed");
        }
        assertFalse(file.canRead());

        //test temporary file
        File directory = File.createTempFile("myDir", "");
        if(!directory.delete()) {
            throw new IOException("test arrangement failed");
        }
        if(!directory.mkdirs()) {
            throw new IOException("test arrangement failed");
        }
        file = new File(directory, "myFile");
        if(!file.createNewFile()) {
            throw new IOException("test arrangement failed");
        }
        if(!file.setReadable(false)) {
            throw new IOException("test arrangement failed");
        }
        assertFalse(file.canRead());
    }
}
